idCompagnie → nomCompagnie
idCompagnie → nomCompagnie, siegeSocial, CA, nomProgFidelite
idCompagnie, idVol, dateHDepart → villeD, villeA,paysD, paysA, aeroportD, aeroportA, terminalD, terminalA, jourHeureArrivee

numeroImaAvion → modele, constructeur, etatDeConcervation, nbTotalPlace, idCompagnie
numeroImaAvion, numSiege → etatSiege, categorie, classe
numeroImaAvion, cat → classe

idClient → nom, prenom, sexe, ddn, adresse, tel, email
numRes, idClient → idCompagnie, idVol, dateHDepart, numSiege
idCompagnie, idVol, dateHDepart, cat → prix, classe
idCompagnie, numVol, dateHDepart, idClient → numSiege

aeroportNom → ville, pays

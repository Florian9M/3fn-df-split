separator = "→"

def transformation(input_file_name, output_file_name):
    res = []
    remove_duplicate = set()
    with open (input_file_name, "r", encoding='utf-8') as f:
        for line in f.readlines():
            res.append(str(line))
            
    output_file = open(output_file_name, "w", encoding='utf-8')
    
    for df in res:
        if df != "\n":
            left_part = df[:df.index("→")].strip()
            right_part = df[df.index("→"):].strip()
            
            right_df_split = right_part.split()
            if len(right_df_split) > 1:
                for under_df in right_df_split:
                    if under_df != separator:
                        new_df = left_part + " " + separator + " " +  under_df.replace(",", "")
                        if new_df not in remove_duplicate:
                            remove_duplicate.add(new_df)
                            print(new_df)
                            output_file.write(new_df + "\n")
                        else:
                            print(new_df + " déja présente")
            else:
                print(left_part + " " + right_part)
                new_df = left_part + " " + right_part + "\n"
                if new_df not in remove_duplicate:
                    remove_duplicate.add(new_df)
                    output_file.writelines(new_df)
                else:
                    print(new_df + " déja présente")

transformation("input.txt", "output.txt")
